
from random import randint
guessNum = ["Guess 1", "Guess 2", "Guess 3", "Guess 4", "Guess 5"]
name = input("What is your name?  >>")
month = randint(1, 12)
year = randint(1924, 2004)
for guess in guessNum:
    month = randint(1,12)
    guessans = input(f"{guess}: {name} were you born in {month}/{year}? 'Y'|'N'|'earlier'|'later'  >>").lower()
    if guessans == "earlier":
        if guess == "Guess 5":
            print("I'll try again another time...")
            exit()
        print("I'm getting closer...")
        yearearly = int(year)
        year = randint(1924, yearearly)
    elif guessans == "later":
        if guess == "Guess 5":
            print("I'll try again another time...")
            exit()
        print("I'm getting closer...")
        yearlate = int(year)
        year = randint(yearlate, 2004)
    elif guessans == "y":
        print("I knew it!")
        exit()
    elif guessans == "n":
        if guess == "Guess 5":
            print("I'll try again another time...")
        else:
            year = randint(1924, 2004)
            print("Drat! I'll just try again!")
    else:
        print("I have other things to do, later!")
        exit()
